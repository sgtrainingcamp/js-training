const sum = (a, b) => a + b;
const customBind = () => {
    /*
    Implement function that will simulate bind function
        @param determine by yourself
        @return {function} function with passed params and with binded context
    */

    // *** YOUR CODE START HERE ***
    // *** YOUR CODE END HERE ***
};
sum.customBind(null, 3, 5)(); // expected return 8


// TODO: Decorations

/*
Theory:
 - Can a function be rebound?
 - Does bind create a new object?
*/

