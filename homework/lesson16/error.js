
/*
Theory:
 - What is
    Error
    EvalError
    RangeError
    ReferenceError
    SyntaxError
    TypeError
    URIError
 - What type has error?
 - What properties has error?
 - Does try/catch work with asynchronous? why?
 - For what finally can be useful?
*/
