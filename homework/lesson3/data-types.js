/*
Create 2 numbers, float and integer;
*/
// *** YOUR CODE START HERE ***
// const float = ...
// const integer = ...
// *** YOUR CODE END HERE ***

const getInfinity = () => {
    /*
    Do some manipulations with them to get:
        infinityValue = Infinity
    */

    // *** YOUR CODE START HERE ***

    // const infinityValue = ...

    // *** YOUR CODE END HERE ***

    return infinityValue;
};

const getMinusInfinity = () => {
    /*
    Do some manipulations with them to get:
        minusInfinityValue = -Infinity
    */

    // *** YOUR CODE START HERE ***

    // const minusInfinityValue = ...

    // *** YOUR CODE END HERE ***

    return minusInfinityValue;
};

const getNaN = () => {
    /*
    Do some manipulations with them to get:
        NaNValue = NaN
    */

    // *** YOUR CODE START HERE ***

    // const NaNValue = ...

    // *** YOUR CODE END HERE ***

    return NaNValue; 
};

/*
Create 2 strings:
    favoritePet = name of favorite pet
    favoritePetSound = sound of favorite pet
*/
// *** YOUR CODE START HERE ***
// export const favoritePet = ...
// export const favoritePetSound = ...
// *** YOUR CODE END HERE ***

const getPetInfo = () => {
    /*
    Create string that will show:
        My favorite pet ***favoritePet**, can do ***favoritePetSound***
    *Try to create string with Backticks that includes Double and single quotes
    */
    // *** YOUR CODE START HERE ***

    // const info = ...

    // *** YOUR CODE END HERE ***

    return info;
};

/*
Print to console:
    -all possible values that return false using !!
    -10 values that return true using !!
    -all possible data types using typeof
*/

/*
Theory:

 -What difference between undefined and null;
*/

module.exports = {
    float,
    integer,
    getInfinity,
    getMinusInfinity,
    getNaN,
    favoritePet,
    favoritePetSound,
    getPetInfo,
};