const BooksLib = (books) => {
    /*
    Everything should be implemented in es5 syntax.
    Implement function constructor and return instance of it.
    You should implement book library, array of books will be available from
    params. You instance should have 3 stated: read, wantToRead, currentlyReading.
    You should implement methods:
        - setRead: move books to read state, return true/false if moved successfully
        - getRead: return array of all read books
        - setWantToRead: move books to wantToRead state, return true/false if moved successfully
        - getWantToRead: return array of all wantToRead books
        - setCurrentlyReading: move books to currentlyReading state, return true/false if moved successfully
        - getCurrentlyReading: return array of all currentlyReading books
        - getAllBooks: return array of all books
        @param {object} books - object with props read, wantToRead, currentlyReading, each prop array with books names
        @return {object} instanceof BooksLib
    */

    // *** YOUR CODE START HERE ***
    // *** YOUR CODE END HERE ***
};
BooksLib({ read: ['a'], wantToRead: ['b'], currentlyReading: ['c'] });


/*
Theory:
 - Describe step by step what happened after 'new' operator
 - What difference between __proto__  and prototype
*/