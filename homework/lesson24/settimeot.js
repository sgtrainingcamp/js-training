const debounce = (func, wait = 0) => {
    /*
       Write your own debounce method.
       For details look https://lodash.com/docs/4.17.10#debounce
       Support of leading edge or trailing edge or maxWait optional
    */

    // *** YOUR CODE START HERE ***
    // *** YOUR CODE END HERE ***
};

const throttle = (func, wait = 0) => {
    /*
       Write your own throttle method
       https://lodash.com/docs/4.17.10#throttle
       Support of leading edge or trailing edge optional
    */

    // *** YOUR CODE START HERE ***
    // *** YOUR CODE END HERE ***
};


/*
Theory:
 - What is propose of setTimeout(func, 0) ?
 - Describe how this code will work and why ?
     for (var i = 1; i < 5; i++) {
        setTimeout(() => console.log(i), 1000)
    }
 - Write all possible variant how to work properly with setTimeout in loop.
 - What can happen if do not clean setInterval?
*/