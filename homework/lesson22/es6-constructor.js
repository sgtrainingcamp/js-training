const BooksLib = (books) => {
    /*
    Everything should be implemented in es6 syntax.
    Implement function constructor and return instance of it.
    You should implement book library, array of books will be available from
    params. You instance should have 3 stated: read, wantToRead, currentlyReading.
    You should implement methods:
        - setRead: move books to read state, return true/false if moved successfully
        - getRead: return array of all read books
        - setWantToRead: move books to wantToRead state, return true/false if moved successfully
        - getWantToRead: return array of all wantToRead books
        - setCurrentlyReading: move books to currentlyReading state, return true/false if moved successfully
        - getCurrentlyReading: return array of all currentlyReading books
        - getAllBooks: return array of all books
        @param {object} books - object with props read, wantToRead, currentlyReading, each prop array with books names
        @return {object} instanceof BooksLib
    */

    // *** YOUR CODE START HERE ***
    // *** YOUR CODE END HERE ***
};
BooksLib({ read: ['a'], wantToRead: ['b'], currentlyReading: ['c'] });

const classInheritance = () => {
    /*
    Please rewrite es6 block of code to es5.
    Code that should be rewritten:
        class Animal {
            constructor (nickname, sex) {
                this.nickname = nickname
                this.sex = sex
                this.changeNickname(nickname)
            }
            changeNickname (nickname) {
                this.nickname = nickname
            }
        }
        class Dog extends Animal {
            constructor (nickname, sex, bark) {
                super(nickname, sex)
                this.bark  = bark
            }
        }
        class Cat extends Animal {
            constructor (nickname, sex, meow) {
                super(nickname, sex)
                this.meow  = meow
            }
        }
        @return {object} {
            Animal: instanceof Animal,
            Dog: instanceof Dog,
            Cat: instanceof Cat
        }
    */

    // *** YOUR CODE START HERE ***
    // *** YOUR CODE END HERE ***
};

/*
Theory:
 - What is Static Members in es6, what equivalent can be at es5?
 - What is Getter/Setter  in es6, what equivalent can be at es5?
*/

