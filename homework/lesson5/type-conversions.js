
let a = '2';

//use variable b to make c = 100

let b = '0';

let c = 10;

//make e = false, f = true

let e = 0;
let f = 0;

//set operator between operands in that way that 'false' will be equal '0'
//console.log(false ? 0)

//set operator between operands in that way that 'null' will be not equal '0'
//console.log(null ? 0)

//cast r to string value

let r = 0;

//cast next variables to numeric values

let s = null;

let t;

let v = NaN;